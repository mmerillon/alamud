# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action3
from mud.events import ScreamEvent

class ScreamAction(Action3):
    EVENT = ScreamEvent
    ACTION = "scream"
    RESOLVE_OBJECT = "resolve_for_operate"

    def __init__(self, subject, object):
        super().__init__(subject, "porte", object)

    def resolve_object2(self):
        return self.object2